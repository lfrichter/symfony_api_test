<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{

    /**
     * @Route("/")
     * @return Response
     */
    public function homepage()
    {
        return new Response('Response!!');
    }

    /**
     * @Route("/new/{slug}")
     */
    public function show($slug)
    {
        $title = ucwords(str_replace('-',' ', $slug));
        $comments = [
            'Loren Gray Beech known professionally as Loren Gray, is an American singer and social media personality ',
            'from Pottstown, Pennsylvania with more than 36 million TikTok followers,'.
            ' over 18 Million Instagram followers, and 3.6 million YouTube subscribers as of August 2019',
            'In computer programming, a comment is a programmer-readable explanation or annotation in the...',
            'e source code of a computer program. They are added with the purpose of making the source code easier ',
            'for humans to understand, and are generally ignored by compilers and interpreters.',
        ];

        dump($slug,$comments, $this);
        return $this->render('article/show.html.twig', compact('title','slug', 'comments'));
    }

}